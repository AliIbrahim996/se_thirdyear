package AirPlane;

public class Employee {
    private int id, age, phoneNumber;
    private String name;

    public Employee(int id, int age, int phoneNumber, String name) {
        this.id = id;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.name = name;
    }

    public String printInfo() {
        return "Employee Info:\n" + "Id: " + this.id +
                "\nName: " + this.name + "\nAge: " + this.age + "\nphoneNumber: " + this.phoneNumber;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setName(String name) {
        this.name = name;
    }
}
