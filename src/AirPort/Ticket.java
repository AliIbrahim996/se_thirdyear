package AirPort;

public abstract class Ticket {

    private int id;
    protected double price;
    private Customer c;

    public int getId() {
        return id;
    }

    public Customer getC() {
        return c;
    }

    public void setC(Customer c) {
        this.c = c;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public abstract void setPrice(double price);
}
