package AirPlane;

import java.util.ArrayList;
import java.util.List;

public class AirPlane {
    private final List<Component> components;
    private int id;
    private Team team;

    public AirPlane() {
        components = new ArrayList<>();
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public void addComponent(Component c) {
        components.add(c);
    }

    public List<Component> getComponents() {
        return components;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
