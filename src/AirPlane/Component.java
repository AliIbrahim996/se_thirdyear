package AirPlane;

import java.util.ArrayList;
import java.util.List;

public class Component {

    final List<Component> components;

    public Component() {
        this.components = new ArrayList<>();
    }

    public void addComponent(Component c) {
        components.add(c);
    }

    public List<Component> getComponents(){
        return this.components;
    }
}
