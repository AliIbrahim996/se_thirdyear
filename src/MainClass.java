import AirPlane.AirPlane;
import AirPlane.Component;
import AirPlane.Employee;
import AirPlane.Team;
import AirPort.*;

public class MainClass {
    public static void main(String argc[]) {

        //Employees
        Employee ali = new Employee(1, 25, 12345, "Ali");
        Employee opada = new Employee(2, 24, 6789, "Opada");
        Employee fouad = new Employee(3, 25, 1357, "Fouad");
        Employee maram = new Employee(4, 24, 2468, "Maram");
        Employee manager = new Employee(5, 35, 101010, "Manager");


        //Teams
        Team t1 = new Team();
        t1.addEmployee(ali);
        t1.addEmployee(fouad);
        Team t2 = new Team();
        t2.addEmployee(opada);
        t2.addEmployee(maram);

        Team airPlaneTeam = new Team();
        airPlaneTeam.addSubTeam(t1);
        airPlaneTeam.addSubTeam(t2);
        airPlaneTeam.addEmployee(manager);


        //Components
        Component planeEngine = new Component();

        Component planeWings = new Component();
        Component planeWing1 = new Component();
        Component planeWing2 = new Component();
        Component planeBackWing = new Component();

        planeWings.addComponent(planeWing1);
        planeWings.addComponent(planeWing2);
        planeWings.addComponent(planeBackWing);

        Component planeSeat1 = new Component();
        Component planeSeat2 = new Component();
        Component planeSeat3 = new Component();
        Component planeSeat4 = new Component();

        Component planeSeats = new Component();
        planeSeats.addComponent(planeSeat1);
        planeSeats.addComponent(planeSeat2);
        planeSeats.addComponent(planeSeat3);
        planeSeats.addComponent(planeSeat4);

        //Building airplane
        AirPlane airPlane = new AirPlane();
        airPlane.addComponent(planeEngine);
        airPlane.addComponent(planeSeats);
        airPlane.addComponent(planeWings);

        airPlane.setTeam(airPlaneTeam);
        airPlane.setId(101);
        //Todo print plane component and team members info

        //Air Port
        Customer customer1 = new Customer();
        Customer customer2 = new Customer();
        customer1.setName("customer1");
        customer1.setType("normal");
        customer2.setName("customer2");
        customer2.setType("super");

        Ticket ticket1 = new NormalTicket();
        Ticket ticket2 = new BusinessClass();
        Ticket ticket3 = new BusinessClass();
        Ticket ticket4 = new BusinessClass();

        ticket1.setId(111);
        ticket1.setC(customer1);
        ticket1.setPrice(100);

        ticket2.setId(222);
        ticket2.setC(customer1);
        ticket2.setPrice(100);

        ticket3.setId(2221);
        ticket3.setC(customer2);
        ticket3.setPrice(120);

        ticket2.setId(2222);
        ticket2.setC(customer2);
        ticket2.setPrice(120);

        Flight f1 = new Flight();
        f1.addTicket(ticket1);
        f1.addTicket(ticket2);

        AirPort lattakia = new AirPort();
        lattakia.setCode("151615");
        lattakia.setCity("Jableh");
        lattakia.setCountry("Syria");
        lattakia.setName("Albasel international airport");

        AirPort damascus = new AirPort();
        damascus.setName("Damascus international  airport");
        damascus.setCity("Damascus");
        damascus.setCountry("Syria");
        damascus.setCode("205151");

        f1.setArrivalsAirPort(damascus);
        f1.setDepAirPort(lattakia);

        Flight f2 = new Flight();
        f2.addTicket(ticket3);
        f2.addTicket(ticket4);

        AirPort aleppo = new AirPort();
        aleppo.setName("Aleppo international  airport");
        aleppo.setCity("Aleppo");
        aleppo.setCountry("Syria");
        aleppo.setCode("213165");

        f2.setDepAirPort(aleppo);
        f2.setArrivalsAirPort(lattakia);



    }
}
