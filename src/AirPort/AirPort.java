package AirPort;

import AirPlane.AirPlane;

import java.util.ArrayList;
import java.util.List;

public class AirPort {
    private String name, code, country, city;
    private List<AirPlane> airPlanes;

    public AirPort(){
        airPlanes = new ArrayList<>();
    }
    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String printAirPortInfo() {
        return "AirPort -> " + this.name + "\n" +
                "Country -> " + this.country + "\n" +
                "City -> " + this.city + "\n" +
                "Code -> " + this.code + "\n";
    }
}
