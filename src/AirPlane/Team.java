package AirPlane;

import java.util.ArrayList;
import java.util.List;

public class Team {
    private List<Employee> employees;
    private List<Team> subTeams;
    int id;

    public Team() {
        employees = new ArrayList<>();
        subTeams = new ArrayList<>();
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public void addSubTeam(Team team){
        subTeams.add(team);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public List<Team> getSubTeams() {
        return subTeams;
    }
}
