package AirPort;

import java.util.ArrayList;
import java.util.List;

public class Flight {
    private List<Ticket> tickets;
    private AirPort depAirPort;
    private AirPort arrivalsAirPort;

    public Flight() {
        tickets = new ArrayList<>();
    }

    public void setDepAirPort(AirPort depAirPort) {
        this.depAirPort = depAirPort;
    }

    public void setArrivalsAirPort(AirPort arrivalsAirPort) {
        this.arrivalsAirPort = arrivalsAirPort;
    }

    public void addTicket(Ticket t) {
        tickets.add(t);
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public int getTicketsCount() {
        return tickets.size();
    }
}
